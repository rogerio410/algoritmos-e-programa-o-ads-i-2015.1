#Abrir o arquivo
arq = open('alunos.txt')

#Criar uma lista de alunos removendo o '\n'
alunos = []
for l in arq:
	alunos.append(l.strip())

#calculos
soma_idade_m = 0.0
soma_idade_f = 0.0
qtd_m = 0
qtd_f = 0
qtd_menores = 0

for a in alunos:
	dados = a.split(';')
	nome = dados[0]
	sexo = dados[1]
	idade = float(dados[2])

	#qtd de menores
	if idade < 18:
		qtd_menores += 1

	#Qtd e somas de idade por sexo
	if sexo == 'M':
		qtd_m += 1
		soma_idade_m += idade
	else:
		qtd_f += 1
		soma_idade_f += idade

#Resultados
#total = qtd_f + qtd_m
total = len(alunos)
perc_m = float(qtd_m)/total*100
perc_f = float(qtd_f)/total*100
media_idade = (soma_idade_f + soma_idade_m)/total

#imprimir Resultados
print "Masculino %d >> %.2f %%" % (qtd_m, perc_m)
print "Feminino %d >> %.2f %%" % (qtd_f, perc_f)
print "Media de Idade: %.2f" % media_idade
print "Qtd de Menores: %d" % qtd_menores


