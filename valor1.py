def receber_valor(min, max):
	valor = input('Digite Valor[%d-%d]: ' % (min, max))
	while (valor < min) or (valor > max):
		valor = input('Digite Valor Novamente[%d-%d]: ' % (min, max))
		
	return valor

valor_digitado = receber_valor(100, 200)
print valor_digitado