#Ponto
class Ponto(object):
	"""Classe para representar um ponto X, Y"""
	x = 0
	y = 0

class Retangulo(object):
	"""Um retangulo """
	largura = 0
	altura = 0
	origem = None

	def area(self):
		print "Area eh %d" % (self.largura*self.altura)

	def move(r, sentido, passos):
		if sentido == 'h':
			r.origem.x += passos
		else:
			r.origem.y += passos

	def pos(self):
		print "Origem (%d, %d)" % (self.origem.x, self.origem.y)
