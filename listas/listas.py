def contar(lista, item):
	contador = 0
	for i in lista:
		if i == item:
			contador += 1
	return contador

def has_duplicates(lista):
	for e in lista:
		if contar(lista, e) > 1:
			return True
	return False