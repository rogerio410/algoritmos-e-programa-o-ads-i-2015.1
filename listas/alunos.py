def carregar_arquivo(arq):
	fin = open(arq)
	lista = list(fin)
	for i in range(len(lista)):
		lista[i] = lista[i].strip()
	return lista

def has_duplicates_names(lista):
	nomes = []
	for a in lista:
		dados = a.split(';')
		nomes.append(dados[1])
	for n in nomes:
		if nomes.count(n) > 1:
			return True
	return False

def perc_por_sexo(lista):
	qtd_m = 0.0
	qtd_f = 0.0
	qtd = len(lista)
	for a in lista:
		dados = a.split(';')
		if dados[3] == 'M':
			qtd_m += 1
		else:
			qtd_f += 1
	perc_m = (qtd_m/qtd)*100
	perc_f = (qtd_f/qtd)*100
	return "M: %.2f %% F: %.2f %%" % (perc_m, perc_f)