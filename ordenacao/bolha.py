def ordenar(conjunto):
	iteracoes = 0
	for i in range(len(conjunto)-1):
	    for j in range(len(conjunto)-1):
	    	iteracoes += 1
	        if conjunto[j] > conjunto[j+1]:
	            corredor = conjunto[j]
	            conjunto[j] = conjunto[j+1]
	            conjunto[j+1] = corredor
	print "iteracoes: ", iteracoes
	return conjunto

def ordenar_desc(conjunto):
	for i in range(len(conjunto)-1):
	    for j in range(len(conjunto)-1):
	        if conjunto[j] < conjunto[j+1]:
	            corredor = conjunto[j]
	            conjunto[j] = conjunto[j+1]
	            conjunto[j+1] = corredor
	return conjunto

def ordenar_turbo(conjunto):
	iteracoes = 0
	for i in range(len(conjunto)-1):
		trocas = 0
		for j in range(len(conjunto)-1):
			iteracoes += 1
			if conjunto[j] > conjunto[j+1]:
				corredor = conjunto[j]
				conjunto[j] = conjunto[j+1]
				conjunto[j+1] = corredor
				trocas += 1
		if trocas == 0:
			print "iteracoes: ", iteracoes
			return conjunto   	
	print "iteracoes: ", iteracoes
	return conjunto