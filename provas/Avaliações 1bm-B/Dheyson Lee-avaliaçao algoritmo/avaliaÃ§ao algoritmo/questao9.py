x = float(input())
n1 = x / 100
x = x % 100
n2 = x / 50
x = x % 50
n3 = x / 20
x = x % 20
n4 = x / 10
x = x % 10
n5 = x / 5
x = x % 5
n6 = x / 2
x = x % 2
n7 = x / 1.00
x = x % 1.00
n8 = x / 0.50
x = x % 0.50
n9 = x / 0.25
x = x % 0.25
n10 = x / 0.10
x = x % 0.10
n11 = x / 0.05
x = x % 0.05
n12 = x / 0.01
x = x % 0.01
print "NOTAS:"
print "%d nota(s) de R$ 100,00" % n1
print "%d nota(s) de R$ 50,00" % n2
print "%d nota(s) de R$ 20,00" % n3
print "%d nota(s) de R$ 10,00" % n4
print "%d nota(s) de R$ 5,00" % n5
print "%d nota(s) de R$ 2,00" % n6
print "MOEDAS:"
print "%d nota(s) de R$ 1,00" % n7
print "%d nota(s) de R$ 0,50" % n8
print "%d nota(s) de R$ 0,25" % n9
print "%d nota(s) de R$ 0,10" % n10
print "%d nota(s) de R$ 0,05" % n11
print "%d nota(s) de R$ 0,01" % n12
