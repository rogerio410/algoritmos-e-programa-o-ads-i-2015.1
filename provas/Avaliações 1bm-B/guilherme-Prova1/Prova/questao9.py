valor_inicial = input("Digite o valor ")
nota_cem = valor_inicial//100
valor_resto = valor_inicial % 100

nota_cinquent = valor_resto //50
valor_resto = valor_resto % 50
nota_vinte = valor_resto//20
valor_resto = valor_resto % 20
nota_dez = valor_resto//10
valor_resto = valor_resto % 10
nota_cinco = valor_resto//5
valor_resto = valor_resto % 5
nota_dois = valor_resto//2
valor_resto = valor_resto % 2

uma_moeda = valor_resto *100

moeda_cem = uma_moeda//100
uma_moeda = uma_moeda % 100

moeda_cinquenta = uma_moeda //50
uma_moeda = uma_moeda % 50

moeda_vintcinco = uma_moeda //25
uma_moeda = uma_moeda % 25

moeda_dez = uma_moeda //10
uma_moeda = uma_moeda % 10

moeda_cinco = uma_moeda //5
uma_moeda = uma_moeda % 5

moeda_um = uma_moeda//1
uma_moeda = uma_moeda % 1

print "%.2f"%valor_inicial
print "NOTAS"
print "%.2f nota(s) de R$ 100,00"%nota_cem
print "%.2f nota(s) de R$ 50,00"%nota_cinquent
print "%.2f nota(s) de R$ 20,00"%nota_vinte
print "%.2f nota(s) de R$ 10,00"%nota_dez
print "%.2f nota(s) de R$ 5,00"%nota_cinco
print "%.2f nota(s) de R$ 2,00"%nota_dois

print "MOEDAS:"
print "%d moeda(s) de R$ 1.00"%moeda_cem
print "%d moeda(s) de R$ 0.50"%moeda_cinquenta
print "%d moeda(s) de R$ 0.25"%moeda_vintcinco
print "%d moeda(s) de R$ 0.10"%moeda_dez
print "%d moeda(s) de R$ 0.05"%moeda_cinco
print "%d moeda(s) de R$ 0.01"%moeda_um

