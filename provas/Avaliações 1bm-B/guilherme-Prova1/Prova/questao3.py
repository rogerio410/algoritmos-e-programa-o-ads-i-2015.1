def valida_Nota(nota):

	if(nota > 10.0 or nota < 0.0):
		print "nota invalida"
		return False
	else:
		return True

qtd_notas, media = 0, 0.0

while(qtd_notas < 2):
	nota = input()

	if (valida_Nota(nota)):
		media += nota
		qtd_notas += 1

media = media / 2.0

print "media = %.2f"  % media

