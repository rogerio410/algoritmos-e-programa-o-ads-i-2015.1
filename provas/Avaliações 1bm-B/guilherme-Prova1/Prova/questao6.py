qtd_C, qtd_R, qtd_S, qtd_cobaias = 0, 0, 0, 0

n = input()

for i in range(0, n):
	valores = raw_input()

	quantia, tipo = map(str, valores.split())
	qtd_cobaias += int(quantia)

	if(tipo == 'C'):
		qtd_C += int(quantia)
	elif(tipo == 'R'):
		qtd_R += int(quantia)
	elif(tipo == 'S'):
		qtd_S += int(quantia)

percento_C = (qtd_C * 100.0) / float(qtd_cobaias)
percento_S = (qtd_S * 100.0) / float(qtd_cobaias)
percento_R = (qtd_R * 100.0) / float(qtd_cobaias)  

print "Total: %d cobaias" % qtd_cobaias
print "Total de coelhos: %d" % qtd_C
print "Total de ratos: %d" % qtd_R
print "Total de sapos: %d" % qtd_S
print "Percentual de coelhos: %.2f %%" % percento_C
print "Percentual de ratos: %.2f %%" % percento_R
print "Percentual de sapos: %.2f %%" % percento_S
