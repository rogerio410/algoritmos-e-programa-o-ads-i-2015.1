tcoelhos=0
tratos=0
tsapos=0
n=int(input("Digite o numero de teste: "))
for i in range(0,n):
	quantia= int(input("Digite a quantia: "))
	while not(quantia>=1 and quantia<=15):
		print("Quantia invalida")
		quantia= int(input("Digite a quantia: "))
	tipo=input("Digite o tipo (C, R ou S): ")
	while not(tipo=="C" or tipo=="R" or tipo=="S"):
		print("Tipo invalido")
		tipo= str(input("Digite o tipo (C, R ou S): "))
	if tipo=="C":
		tcoelhos=tcoelhos+quantia
	elif tipo=="R":
		tratos=tratos+quantia
	else:
		tsapos=tsapos+quantia
total=tcoelhos+tratos+tsapos
print("Total: %d cobaias" % total)
print("Total de coelhos: ", tcoelhos)
print("Total de ratos: ", tratos)
print("Total de sapos: ", tsapos)
print("Percentual de coelhos: %.2f %%" % ((tcoelhos*100)/total))
print("Percentual de ratos: %.2f %%" % ((tratos*100)/total))
print("Percentual de sapos: %.2f %%" % ((tsapos*100)/total))
