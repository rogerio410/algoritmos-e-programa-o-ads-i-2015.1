valor = float(raw_input())

print 'NOTAS:'

for i in (100,50,20,10,5,2):

    if (valor >=i):
        cedulas = valor/i
        valor = valor %i
        print '%d nota(s) de R$ %.2f' % (cedulas,i)
 
    else:
        print '0 nota(s) de R$ %.2f'% i

print 'MOEDAS:'

for x in (1,0.50,0.25,0.10,0.05,0.01):

    if (valor >=x):
        moedas = valor/x
        valor = valor %x
        print '%d moeda(s) de R$ %.2f' % (moedas,x)
 
    else:
        print '0 moeda(s) de R$ %.2f'% x
