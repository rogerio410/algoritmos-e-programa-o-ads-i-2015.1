correcoes = open("correcao_prova.csv")
alunosenotas = []
for linha in correcoes:
	nomeenotas = []
	nomeenotas = linha.strip().split(";")
	total = 0	
	for nota in range(1, 4):
		nomeenotas[nota] = nomeenotas[nota].replace("%", "")
		nomeenotas[nota] = int(nomeenotas[nota])
		nomeenotas[nota] = nomeenotas[nota]*30/10000.0 * 1
		total += nomeenotas[nota]
	for nota in range(4, 6):
		nomeenotas[nota] = nomeenotas[nota].replace("%", "")
		nomeenotas[nota] = int(nomeenotas[nota])
		nomeenotas[nota] = nomeenotas[nota]*30/10000.0 * 2
		total += nomeenotas[nota]
	for nota in range(6, 7):
		nomeenotas[nota] = nomeenotas[nota].replace("%", "")
		nomeenotas[nota] = int(nomeenotas[nota])
		nomeenotas[nota] = nomeenotas[nota]*30/10000.0 * 3
		total += nomeenotas[nota]
	for nota in range(7, 10):
		nomeenotas[nota] = nomeenotas[nota].replace("%", "")
		nomeenotas[nota] = int(nomeenotas[nota])
		nomeenotas[nota] = nomeenotas[nota]*70/10000.0 * 1
		total += nomeenotas[nota]
	for nota in range(10, 12):
		nomeenotas[nota] = nomeenotas[nota].replace("%", "")
		nomeenotas[nota] = int(nomeenotas[nota])
		nomeenotas[nota] = nomeenotas[nota]*70/10000.0 * 2
		total += nomeenotas[nota]
	for nota in range(12, 13):
		nomeenotas[nota] = nomeenotas[nota].replace("%", "")
		nomeenotas[nota] = int(nomeenotas[nota])
		nomeenotas[nota] = nomeenotas[nota]*70/10000.0 * 3
		total += nomeenotas[nota]
	if total >= 7:
		print nomeenotas[0], "-- Nota:", total, "Aprovado"
	elif total < 7 and total > 4:
		print nomeenotas[0], "-- Nota:", total, "Prova Final"
	elif total < 4:
		print nomeenotas[0], "-- Nota:", total, "Reprovado"
