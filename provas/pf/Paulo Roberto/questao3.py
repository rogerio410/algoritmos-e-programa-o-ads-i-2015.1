arquivo = open("populacao_estimada.csv")
cidades = []
def cmpporpop(x, y):
	return cmp(x[2], y[2])
for cidade in arquivo:
	cidades.append(cidade.strip().split(";"))
for cidade in range(len(cidades)):
	cidades[cidade][2] = cidades[cidade][2].replace(".", "")
	cidades[cidade][2] = int(cidades[cidade][2])
def menu():
	print "---------------IBGE - Populacao por cada cidade---------------"
	print "1 - Total da populacao brasileira"
	print "2 - Total de populacao estimada por estado"
	print "3 - Consulta por municipio"
	print "4 - Municipios mais populosos"
	print "5 - Municipios menos populosos"
	print "6 - Municipios mais populosos por UF"
	print "7 - Total de municipios de uma UF"
	opcao = input("Selecione uma opcao: ")
	if opcao == 1:
		totaldobrasil = 0
		for cidade in range(len(cidades)):
			totaldobrasil += cidades[cidade][2]
		print totaldobrasil
	if opcao == 2:
		totaluf = 0
		uf = raw_input("Insira a sigla do estado desejado: ")
		uf = uf.upper()
		for cidade in range(len(cidades)):
			if cidades[cidade][0] == 'df':
				totaluf += cidades[cidade][2]
		print totaldf
	if opcao == 3:
		municipio = raw_input("Insira o nome ou parte do nome do municipio: ")
		for cidade in range(len(cidades)):
			if municipio.lower() in cidades[cidade][1].lower():
				print cidades[cidade][1], " - ", cidades[cidade][2]
	if opcao == 4:
		cidades.sort(cmpporpop, reverse = True)
		for cidade in range(1, 11):
			print cidades[cidade][1],"(",cidades[cidade][0],")", cidades[cidade][2]
	if opcao == 5:
		cidades.sort(cmpporpop)
		for cidade in range(1, 11):
			print cidades[cidade][1],"(",cidades[cidade][0],")", cidades[cidade][2]
	if opcao == 6:
		df = raw_input("Insira a sigla do estado desejado: ")
		contador = 0
		cidades.sort(cmpporpop, reverse = True)
		for cidade in range(len(cidades)):
			if cidades[cidade][0] == df.upper():
				print cidades[cidade][1],"(",cidades[cidade][0],")", cidades[cidade][2]
				contador += 1
				if contador >= 10:
					break
	if opcao == 7:
		df = raw_input("Insira a sigla do estado desejado: ")
		contador = 0
		cidades.sort(cmpporpop, reverse = True)
		for cidade in range(len(cidades)):
			if cidades[cidade][0] == df.upper():
				contador += 1
		print "Numero de municipios no", df,":", contador
menu()
