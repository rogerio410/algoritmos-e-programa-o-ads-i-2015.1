#inicializando
correcao = open('correcao_prova.csv')
matriz = []
for i in range(23):
	matriz.append(correcao.readline().replace('%', '').split(';'))



#funcoes
def pontos():
	valor = {1:1.0, 2:1.0, 3:1.0, 4:2.0, 5:2.0, 6:3.0, 7:1.0, 8:1.0, 9:1.0, 10:2.0, 11:2.0, 12:3.0}
	for i in range(23):
		somap1, somap2 = 0, 0
		for j in range(1, 7):
			p1 = ((float(matriz[i][j])) * 30 / 100) * valor[j] / 100
			somap1 += p1
		for j in range(7, 13):
			p2 = ((float(matriz[i][j])) * 70 / 100) * valor[j] / 100
			somap2 += p2
		total = somap1 + somap2
		matriz[i].append(total)

def resultado(i):
	if matriz[i][13] >= 7.0:
		return 'Aprovado'
	elif matriz[i][13] < 4.0:
		return 'Reprovado'
	else:
		return 'Prova Final'

def listaOrdenada():
	ordem = []
	for i in range(23):
		ordem.append(float(matriz[i][13]))
	ordem.sort(reverse = True)
	return ordem




#resultado
pontos()
print('\n\n------------------Resultado-----------------------------\n')
for i in range(23):
	ordem = listaOrdenada()
	for j in range(23):
		if matriz[j][13] == ordem[i]:
			indice = j
	print ('%s - %.2f, %s' % (matriz[indice][0], matriz[indice][13], resultado(indice)))
