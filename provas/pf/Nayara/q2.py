
###########################Inicializando########################################

import random
lista = []
quant = 2000000
acumulado = 103000000


##################Preparando os bilhetes e a dezena sorteada####################


def produzirBilhetes():
	for i in range(quant):
		dezena = [random.randint(0,60) for i in range(6)]
		lista.append(dezena)

produzirBilhetes()
entrada = raw_input('Digite as dezenas separadas por espacos: ')
dezenas =[int(i) for i in entrada.split()]


#########################Calculando a pontuacao#################################


quadra, quinta, sena = 0, 0, 0
for i in range(quant):
	pontuacao = 0
	for j in range(6):
		if lista[i][j] == dezenas[j]:
			pontuacao += 1 
	if pontuacao == 4:
		quadra += 1
	elif pontuacao == 5:
		quinta += 1
	elif pontuacao == 6:
		sena += 1

#############################Calculando os valores dos premios###################


recadacao = ((quant * 3.5) + acumulado) * 46 / 100
premioSena = recadacao * 35 / 100 
premioQuinta = recadacao * 19 / 100
premioQuadra = recadacao * 19 / 100


##########################Mostrando o resultado##################################

juntar = 0
print('\nDezenas Sorteadas: %s' % (entrada))
if sena > 0:
	print('\nSENA: %d apostas --> Premio de cada um R$ %.2f' % (sena, premioSena))
	juntar = premioSena
if quinta > 0:
	print('\nQUINTA: %d apostas --> Premio de cada um R$ %.2f' % (quinta, premioQuinta/quinta))
	juntar = premioQuinta
if quadra > 0:
	print('\nQUADRA: %d apostas --> Premio de cada um R$ %.2f' % (quadra, premioQuadra/quadra))
	juntar = premioQuadra
acumulado = (recadacao * 27 / 100) + juntar
print('\nACUMULADO = R$ %.2f' % (acumulado))
print('\n*obs: Caso alguns sorteios nao apareca, para esses nao houver ganhador\n')

