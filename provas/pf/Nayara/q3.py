
#ABERTURA
cidades = open('populacao_estimada.csv','r')
linhas = cidades.read().count('\n')
cidades = open('populacao_estimada.csv','r')
matriz = []
for i in range(linhas):
	matriz.append(cidades.readline().replace('\n', '').replace('\r', '').replace('.', '').split(';'))



#FUNCOES
def listaOrdenada(direcao):
	ordem = []
	for i in range(linhas):
		ordem.append(int(matriz[i][2]))
	ordem.sort(reverse = direcao)
	return ordem

def top10(direcao):
	ordem = listaOrdenada(direcao)
	for i in range(10):
		for j in range(linhas):
			if int(matriz[j][2]) == ordem[i]:
				indice = j
		print('%d - %s - %s - %s' % (i, matriz[indice][0], matriz[indice][1], matriz[indice][2]))
def media():
	soma = 0
	for i in range(50,linhas-50):
		soma += int(matriz[i][2])
	media = soma / (linhas - 100)
	return(media)

def consulta(parte):
	num = 0
	chaves = {}
	for i in range(linhas):
		if matriz[i][1].__contains__(parte.capitalize()):
			num += 1
			chaves[num] = i 
			print('%d - %s - %s' % (num, matriz[i][1], matriz[i][0]))
	if num > 0:
		num = int(input('Digite o numero da cidade: '))
		i = chaves[num]
		print ('--------------------------\n%s - %s - %s\n--------------------------' % (matriz[i][1], matriz[i][0], matriz[i][2]))



#MENU
def menu():
	op = -1
	while op != 0:
		print('\n\n---OPCOES---\n1 - Total de Populacao Brasileira\n2 - Total de Populacao por Estado\n3 - Total de municipios por uf\n4 - Top 10 Municipios mais populosos\n5 - Top 10 Municipios menos populosos\n6 - Media Nacional\n7 - Consultar dados de um municipio\n0 - sair\n\n')
		op = input('Digite sua escolha: ')
		if op == 1:
			total = 0
			for i in range(linhas):
				total += int(matriz[i][2])
			print('-----Total da populacao brasileira: %d' % total)
		elif op == 2:
			totalPEstado = 0
			uf = raw_input('Digite a uf do estado: ').upper()
			for i in range(linhas):
				if matriz[i][0] == uf:
					totalPEstado += int(matriz[i][2])
			print('-----Total de Populacao Estimada do estado: %d' % (totalPEstado))
		elif op == 3:
			quant = 0
			uf = raw_input('Digite a uf do estado: ').upper()
			for i in range(linhas):
				if matriz[i][0] == uf:
					quant +=1
			print('-----Total de municipios: %d' % (quant))
		elif op == 4:
			top10(True)
		elif op == 5:
			top10(False)
		elif op == 6:
			print('-----Media populacional municipal do Brasil: %d ' % media())
		elif op == 7:
			nome = raw_input('Digite nome do municipio: ') 
			consulta(nome)
		elif op == 0:
			print('-----Programa finalizado!')
		else:
			print('-----Opcao invalida! Digite novamente!')
	
menu()
