def consultanome(populacao,nome):
    l = []
    n = []
    m = []
    for i in range(len(populacao)):
        if populacao[i][1].__contains__(nome):
            l.append(populacao[i][0])
            n.append(populacao[i][1])
            m.append(populacao[i][2])
    return l,n,m
def totalUF(populacao,UF):
    UF = UF.upper()
    cont = 0
    for i in range(len(populacao)):
        if UF == populacao[i][0]:
            cont += 1
    return cont

def totalpopUF(populacao,UF):
    UF = UF.upper()
    total = 0
    for i in range(len(populacao)):
        if UF == populacao[i][0]:
            total += int(populacao[i][2])
    return total

def mediapopularbr(populacao):
    totalmunicipios = len(populacao)
    media = 0
    for i in range(len(populacao)):
        media += int(populacao[i][2])
    md = media / totalmunicipios
    return md
        
def mediapopularUF(populacao,UF):
    UF = UF.upper()
    totalmunicipios = 0
    media = 0
    for i in range(len(populacao)):
        if populacao[i][0] == UF:
            media += int(populacao[i][2])
            totalmunicipios += 1
    md = media / totalmunicipios
    return md
        
