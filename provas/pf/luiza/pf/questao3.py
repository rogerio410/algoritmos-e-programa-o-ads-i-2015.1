f = open("populacao_estimada.csv")

dados = []

for i in f:
	dados.append(i.strip().split(";"))
	
for i in range(len(dados)):
	dados[i][2] = int(dados[i][2].replace(".", ""))

#Total da populacao de um estado
def populacao_uf(uf):
	quant = 0
	for i in range(len(dados)):
		if uf in dados[i][0]:
			quant += dados[i][2]
	

	print "O total da populacao do estado de %s eh: %d" %(uf,quant)


#Total da populacao
def total():
	quant = 0
	for i in range(len(dados)):
		quant += dados[i][2]
	

	print "O total da populacao brasileira eh: " + str(quant)

#Total da populacao de um estado
def mun(uf):
	cont = 0
	for i in range(len(dados)):
		if uf in dados[i][0]:
			cont +=1
	

	print "O total de municipio do estado de %s eh: %d" %(uf,cont)

#Consulta municipio
def consulta_municipio(municipio):
	cont = 0
	print "{:^5}{:^15}{:^10}".format("UF","Populacao", "Nome do municipio")
	for i in range(len(dados)):
		if municipio in dados[i][1].upper():
			print "{:^5}{:^15}{:^10}".format(dados[i][0],dados[i][2], str(dados[i][1]))	
	
#municipios mais populosos
def top10municipios():
	troca = 1
	a=0	
	while troca > 0:	
		for i in range(len(dados)-1):
			if dados[i][2] < dados[i+1][2]:
				dados[i][0],dados[i+1][0] = dados[i+1][0],dados[i][0]
				dados[i][1],dados[i+1][1] = dados[i+1][1],dados[i][1]
				dados[i][2],dados[i+1][2] = dados[i+1][2],dados[i][2]
				a +=1
		troca = a
		a=0

	print "{:^5}{:^15}{:^10}".format("UF","Populacao", "Nome do municipio")
	for i in range(10):
		print "{:^5}{:^15}{:^10}".format(dados[i][0],dados[i][2], str(dados[i][1]))
			

#menu
def menu():
	print"######CONSULTA IBGE######"
	print "1-Populacao estimada de um esdato \n2-Total da populacao de um estado \n3-Total de municipio de um estados \n4-Consultar municipio \n5-Top 10 municipios"

	opcao = input("Digite o numero da opcao: ")

	if opcao == 1:
		total()
		menu()
	if opcao == 2:
		uf = raw_input("Digite o nome do estado: ").upper()
		populacao_uf(uf)
		menu()
	if opcao == 3:
		uf = raw_input("Digite o nome do estado: ").upper()
		mun(uf)
		menu()
	if opcao == 4:
		municipio = raw_input("Nome do municipio: ").upper()
		consulta_municipio(municipio)
		menu()
	if opcao == 5:
		top10municipios()
		menu()
	
menu()


