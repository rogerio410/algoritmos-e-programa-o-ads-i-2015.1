def tabuada(n):
    for i in range(1,11):
        result = i * n
        print "%d X %d = %d" % (i, n, result)

n = int(input("Digite um n para a tabuada: "))

if(n > 2 and n < 1000):
    tabuada(n)    
else:
    print "valor invalido"
