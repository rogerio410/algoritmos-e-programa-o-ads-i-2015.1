qtd_sapo, qtd_rato, qtd_coelho, total = 0, 0, 0, 0

n = int(input("Digite a quantidade de casos: "))

if(1 <= n and n <= 15):
    
    for i in range(n):
        entrada = raw_input("Digite a quantidade e o tipo: ")
        qtd, tipo = map(str, entrada.split())
        qtd = int(qtd)
        total += qtd
        if(tipo == 'S'):
            qtd_sapo += qtd
        elif(tipo == 'R'):
            qtd_rato += qtd    
        elif(tipo == 'C'):
            qtd_coelho += qtd 
	
#APRESENTANDO A SAIDA:
    print "Total: %d" % total
    print "Total de coelhos: %d" % qtd_coelho
    print "Total de ratos: %d" % qtd_rato
    print "Total de sapos: %d" % qtd_sapo

    perc_coelho, perc_rato, perc_sapo = float(qtd_coelho*100) / total, float(qtd_rato*100) / total, float(qtd_sapo*100) / total

    print "Percentual de coelhos: %.2f %%" % perc_coelho
    print "Percentual de ratos: %.2f %%" % perc_rato
    print "Percentual de sapos: %.2f %%" % perc_sapo
