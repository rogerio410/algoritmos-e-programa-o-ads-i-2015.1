def retorna_nome(filo, classe, especie):
    if(filo == "vertebrado"):
        if(classe == "ave"):
	    if(especie == "carnivoro"):
                return "aguia"
            elif(especie == "onivoro"): 
                return "pomba"
        elif(classe == "mamifero") :
	    if(especie == "onivoro"):
                return "homem"
            elif(especie == "herbivoro"): 
                return "vaca"
    elif(filo == "invertebrado"):
	if(classe == "inseto"):
            if(especie == "hematofago"):
                return "pulga"
            elif(especie == "herbivoro"): 
                return "lagarta"
        elif(classe == "anelideo") :
            if(especie == "hematofago"):
                return "sanguessuga"
            elif(especie == "onivoro"): 
                return "minhoca"
    return "Animal nao encontrado"
filo = raw_input("Digite o filo: ")
classe = raw_input("Digite a classe: ")
especie = raw_input("Digite a especie: ")
print "Animal: %s" % retorna_nome(filo, classe, especie)  	


