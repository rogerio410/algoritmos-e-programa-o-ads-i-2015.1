pares, impares, positivos, negativos = 0, 0, 0, 0

for i in range(5):
    n = int(input("Digite um numero: "))
    if(n % 2 == 0):
        pares += 1
    if(n > 0):
        positivos += 1
    elif(n < 0):
        negativos += 1

impares = 5 - pares
#SAIDA
print "%d valor(es) par(es)" % pares
print "%d valor(es) impar(es)" % impares
print "%d valor(es) positivo(s)" % positivos
print "%d valor(es) negativo(s)" % negativos

