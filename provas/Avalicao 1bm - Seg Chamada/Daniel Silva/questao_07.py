def return_nivel(veloc):
    if(veloc < 10):
        return 1
    elif(veloc < 20):
        return 2
    else:
        return 3

lista_velozes = []

while(True):
    n = int(input("Digite uma quantidade de casos: "))
    if(n >= 1 and n <= 50):
        valores = raw_input("Digite os %d valores: " % n)
        lista = map(int, valores.split())

        maior = lista[0]
        for i in range(1, len(lista)):
            if(maior < lista[i]):
               maior = lista[i]
    
        lista_velozes.append(return_nivel(maior))
    elif(n < 0):
        break
#IMPRIMINDO O NIVEL PRA CADA QUESTAO:
for i in lista_velozes:
    print i
