def return_maior(a,b,c):
    #FAZENDO SEM LISTA:
    if(a >= b and b >= c):
        return a, b, c
    elif(a >= c and c >= b):
        return a, c, b
    elif(b >= a and a >= c):
        return b, a, c
    elif(b >= c and c >= a):
        return b, c, a
    elif(c >= b and b >= a):
        return c, b, a
    else:
        return c, a, b

def is_triangulo(a, b, c):
    maior, l1, l2 = return_maior(a,b,c)
    #VERIFICAR MAIOR
    return ( maior < (l1 + l2) )

entrada = raw_input("Digite tres valores para os triangulos: ")
a,b,c = map(float, entrada.split())

#PERIMETRO
if(is_triangulo(a,b,c)):
    per = a + b + c
    print "Perimetro = %.1f" % per
#AREA
else:
    area = ((a + b) / 2.0) * c
    print "Area = %.1f" % area
