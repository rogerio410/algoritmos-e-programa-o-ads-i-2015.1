def verifica_quadrante(x,y):
    if(x > 0 and y > 0):
        return "primeiro"
    elif(x < 0 and y > 0):
        return "segundo"
    elif(x < 0 and y < 0):
        return "terceiro"
    else:
        return "quarto"

while(True):
    entrada = raw_input("Digite os valores de x e y: ")
    x, y = map(int, entrada.split())
    if(x == 0 or y == 0):
        break
    print verifica_quadrante(x,y)
