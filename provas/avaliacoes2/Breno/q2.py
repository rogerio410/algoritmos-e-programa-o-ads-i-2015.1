gabarito = open("gabarito.txt","r")
cartoes = open("cartoes.txt","r")

def split(s):#Faz separacao de arquivo em uma lista
	s = [i.strip() for i in s]
	for i in s:
		if i == '':
			s.remove(i)
	return s

def dados(d):#Separa ID#GABARITO
	d = [i.strip().split("#") for i in d]
	for i in d:
		if i == ['']:
			d.remove(i)
	return d
	
gab = split(gabarito)
usr = dados(cartoes)

