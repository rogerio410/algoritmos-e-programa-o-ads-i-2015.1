class Ponto(object):
    def __init__(self,x,y):
        self.x = x
        self.y = y

class Retangulo(object):
    def __init__(self,base,altura):
        self.base = base
	self.altura = altura
	self.origem = Ponto()
    
    def __str__(self):
        return '({},{})'.format(self.origem.x,self.origem.y)

    def area(self):
        return 'Área: {}'.format(self.base*self.altura)

    def move(self,sentido,unidades):
	if sentido == 'h':
            self.origem.x += unidades
        elif sentido == 'v':
            self.origem.y += unidades
        else:
            except('Valor inválido')
    
