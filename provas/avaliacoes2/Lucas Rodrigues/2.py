g = open('gabarito.txt')
c = open('cartoes.txt')
gabarito = str(g.readline())
cartoes = []
for linha in c:
	cartoes.append(linha.split('#'))
def verificar():
	t = len(cartoes)
	gab = []
	for i in gabarito:
		gab.append(i.strip())
	for j in cartoes:
		temp = []
		acertos = 0
		for n in j[1]:
			temp.append(n.strip())
		for o in range(10):
			if gab[o] == temp[o]:
				acertos = acertos + 1
		print "Aluno #%s: %i acerto(s)" %(j[0], acertos)
verificar()
