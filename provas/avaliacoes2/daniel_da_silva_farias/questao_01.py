# -*- coding: utf-8 -*-

#original => lista = map(raw_input("").split(), int)
lista = map(int, raw_input("").split())
n_maior = 0 #numero com maior frequencia
f_maior = 0 #frequencia desse numero acima

for i in range(len(lista)):
    if(i == 0):
        n_maior = lista[i]
        f_maior = lista.count(lista[i])
    else:
        f_atual = lista.count(lista[i])
        if((f_maior < f_atual) or ( (f_maior == f_atual) and (n_maior < lista[i]) )):
            n_maior = lista[i]
            f_maior = f_atual

print n_maior
