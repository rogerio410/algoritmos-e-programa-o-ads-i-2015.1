# -*- coding: utf-8 -*-
from json import dumps
from utils import *

header = "#########SisBolsa#########"
main = '''
#########FUNCOES BASICAS:
1 - Inserir Familia
2 - Listar Todas
3 - Obter a Quantidade de Familias Cadastradas
4 - Editar Familia
5 - Excluir Familia
###########PESQUISAS INDIVIDUAIS:
6 - Pesquisar Membro por CPF
7 - Pesquisar Membro por NOME
8 - Pesquisar Membro por IDADE
###########RELATORIOS:
9 - Listar Familias por UF
10 - Listar Familias por Cidade
11 - Listar Familias por Zona
12 - Rank por Renda
13 - Rank por Quantidade de Membros
0 - Sair
'''
footer = "#########HEHEHE#########\nO que deseja fazer?\n"
familia = []

def edita_familia(familias, ide):
    print "Editando a familia %s :" % ide
    familias[int(ide)] = colhe_dados_familia()
    print "Familia Atualizada com sucesso"

def colhe_dados_familia():
    renda_mensal = raw_input("\nDigite a renda mensal da familia: ")
    zona_cidade = raw_input("Digite a Zona pertencente a familia (U - urbana, R - rural): ")
    cidade = raw_input("Digite a cidade pertencente a familia: ")
    estado = raw_input("Digite o estado pertencente a familia: ")

    lista_membros = colhe_dados_membro()
	
    lista = [lista_membros, renda_mensal, zona_cidade, cidade, estado]
    
    return lista

def colhe_dados_membro():
    lista = []
    while(True):
        op = raw_input("\nDeseja cadastrar membro? (S/N)")
        if(op == "S"):
            tipo = raw_input("Escreva a funcao do membro: ")
            nome = raw_input("Escreva o nome do membro: ")
            cpf = raw_input("Escreva o cpf do membro: " )
            idade = raw_input("Escreva a idade do membro: ")
	    lista.append([tipo, nome, cpf, idade])
        elif(op == "N"):
            break
	
    return lista 

def show(lista, member = None):
    #Formatando a exibicao de resultados
    if(len(lista) == 0):
        print "Sem resultados para a Busca"
    else:	
        cont = 1
        if(member):    
            print "####Exibicao de Membros####"
            for m in lista:
                print "Numero: %s, Tipo de Membro: %s, Nome: %s, CPF: %s, Idade: %s" % (cont, m[0], m[1], m[2], m[3])
                cont += 1
        else:
            print "####Exibicao de Familias####"
            for f in lista:
                print " Familia: %s - Renda: R$ %.2f - Zona: %s - Cidade: %s - Estado %s" % (cont, float(f[1]), zonas[f[2]], f[3], f[4])
                for m in f[0]:
                    print " - Membro: %s, Nome: %s, CPF: %s, Idade: %s" % (m[0], m[1], m[2], m[3])
                cont += 1

def clear(): #CLEAR UNIVERSAL :)
    for i in range(40):
        print ""

def ler_arquivo(filename = "teste.txt"):
    temp = []
    f = open(filename, "r")

    for i in f:
        temp = i.strip()

    f.close()

    return temp

def sync_familias(familias, filename = "teste.txt"):
    f = open(filename, "w")

    f.write(serialize(familias))
    
    f.close()
    
    print "Bye"

#RECOLHE DADOS DA FAMILIA:
familias = eval(ler_arquivo())
#
while(True):
    op = raw_input(header+main+footer)
    clear()

    if(op == "1"):
        familias.append(colhe_dados_familia())
    elif(op == "2"):
        show(list_by(familias))
    elif(op == "3"):
        print "Quantidade de familias cadastradas: %d" % len(familias)                
    elif(op == "4"):
        codigo = raw_input("Digite o Id da familia para editar: ")
        edita_familia(familias, codigo)                 
    elif(op == "5"):
        codigo = raw_input("Digite o Id da familia para excluir: ")
        exclui_familia(familias, codigo)
    #PESQUISAS INDIVIDUAIS:
    elif(op == "6"):
        cpf = raw_input("Digite a CPF a pesquisar: ")
        show(list_member_by(familias, cpf = cpf), member = True)
    elif(op == "7"):
        nome = raw_input("Digite a NOME a pesquisar: ")
        show(list_member_by(familias, nome = nome), member = True)
    elif(op == "8"):
        idade = raw_input("Digite a IDADE a pesquisar:")
        show(list_member_by(familias, idade = idade), member = True)
    #RELATORIOS:
    elif(op == "9"):
        uf = raw_input("Digite a UF a pesquisar: ")
        show(list_by(familias, estado = uf))
    elif(op == "10"):
        cid = raw_input("Digite a CIDADE a pesquisar: ")
        show(list_by(familias, cidade = cid))
    elif(op == "11"):
        zone = raw_input("Digite a ZONA a pesquisar (U - Urbana, R - Rural): ")
        show(list_by(familias, zona = zone))
    elif(op == "12"):
        show(rank_by(familias, renda = True))
    elif(op == "13"):
        show(rank_by(familias, qtd = True))
    elif(op == "0"):
        break        
    else:
        print "Opcao Invalida Tente Novamente"

#Ao SAIR GRAVAR NA FAMILIA:
sync_familias(familias)
