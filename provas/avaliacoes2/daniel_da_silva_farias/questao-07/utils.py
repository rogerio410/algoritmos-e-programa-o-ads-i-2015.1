# -*- coding: utf-8 -*-
from json import dumps

zonas = {'U': 'Urbana', 'R': 'Rural'}

def exclui_familia(ide):
    op = raw_input("Deseja Mesmo Excluir O Registro %s de Familias? (S/N) " % ide)
    if(op == "S"):
        familias.__delitem__(int(ide))
        print "Familia Excluida com Sucesso!"
    else:
        print "Ok, remocao cancelada."

def sort_by_qtd(lista):
    return len(lista[0])

def sort_by_renda(lista):
    return lista[1]

def rank_by(lista, qtd = None, renda = None):
    if(qtd):
        return sorted(lista, key=sort_by_qtd, reverse=True)
    if(renda):
        return sorted(lista, key=sort_by_renda, reverse=True)

def list_member_by(familias, cpf = None, nome = None, idade = None):
    lista = []
    
    dicionario = {'nome' : 1, 'cpf': 2, 'idade': 3}
    
    busca, search = None, None
	    
    if(nome):
        busca = dicionario['nome']
        search = nome
    if(cpf):
        busca = dicionario['cpf']
        search = cpf
    if(idade):
        busca = dicionario['idade']
        search = idade

    for f in familias:
        for m in f[0]:
            if(m[busca] == search):
                lista.append(m)

    return lista

def list_by(familias, zona = None, estado = None, cidade = None, qtd_membros = None, n = None):
    lista = familias
    
    if(qtd_membros):
        temp = []
        for i in lista:
            if(qtd_membros <= len(i[0])):
                temp.append(i)
        lista = temp

    if(estado or cidade or zona):
        dicionario = {'estado' : 4, 'cidade': 3, 'zona': 2}
        busca, search = None, None

        if(estado):
            busca = dicionario['estado']
            search = estado
        if(cidade):
            busca = dicionario['cidade']
            search = cidade
        if(zona):
            busca = dicionario['zona']
            search = zona

        temp = []

        for i in lista:
            if(search.lower() == i[busca].lower()):
                temp.append(i)

        lista = temp

    return lista

def serialize(family):
    return dumps(family)
