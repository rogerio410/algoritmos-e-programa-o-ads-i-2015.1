# -*- coding: utf-8 -*-

#LINHA ADICIONADA PARA TESTE => L.A.T
#from questao_05 import le_arquivo #L.A.T

def le_arquivo(filename):
    temp = []
    f = open(filename)
    for i in f:
        temp.append(i.strip())
    f.close()
    return temp

#from utils import le_arquivo #[esta assim no papel] ACIMA IMPORTA A FUNCAO DA QUESTAO ANTERIOR, 
palavras = le_arquivo('arquivo.txt')

def lista_repetidos(lista):
    repetidos = []
    tuplas = []
    for i in lista:
        if((not i in repetidos) and (lista.count(i) > 1)): #if((i in repetidos == False) and (lista.count(i) > 1)): error
            tuplas.append((i, lista.count(i)))
	    repetidos.append(i) #Faltou isso na questao do papel	
    return tuplas

def sort_by_frequencia(lista):
    return lista[1]

#ordenadas = sorted(palavras, key=sort_by_frequencia, reverse=True) -> Original com error, criei a funcao lista_repetidos, mas nao usei

ordenadas = sorted(lista_repetidos(palavras), key=sort_by_frequencia, reverse=True)

print ordenadas

for i in ordenadas:
    #print "%s aparece %s vezes" % (ordenadas[0], ordenadas[1]) #ERROR
    print "%s aparece %s vezes" % (i[0], i[1])


