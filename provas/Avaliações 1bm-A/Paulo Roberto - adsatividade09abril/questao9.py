n=input()
if 0<n<1000000:
	n100=int(n/100)
	n50=int(n%100/50)
	n20=int((n50%50)/20)
	n10=int((n20%20)/10)
	n5=int((n10%10)/5)
	n2=int((n5%5)/2)
	m1=int((n2%2)/1)
	m50=int((m1%1)/0.5)
	m25=int((m50%0.5)/0.25)
	m10=int((m25%0.25)/0.10)
	m5=int((m10%10)/0.5)
	m1=int((m5%5)/1)
print"Notas:"
print n100, "Nota(s) de 100"
print n50, "Nota(s) de 50"
print n20, "Nota(s) de 20"
print n10, "Nota(s) de 10"
print n5, "Nota(s) de 5"
print n2, "Nota(s) de 2"
print"Moedas:"
print m1, "Moeda(s) de 1"
print m50, "Moeda(s) de 50"
print m25, "Moeda(s) de 25"
print m10, "Moeda(s) de 10"
print m5, "Moeda(s) de 5"
print m1, "Moeda(s) de 1"
