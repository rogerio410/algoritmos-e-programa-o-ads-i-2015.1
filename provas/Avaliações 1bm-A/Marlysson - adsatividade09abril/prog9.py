# -*- coding: utf8 -*-

valor = float(input('Valor: '))

print 'CÉDULAS:'
for i in [100,50,20,10,5,2]:
	notas = valor // i
	valor -= notas*i
	print '{} Notas de R$ {:.2f}'.format(int(notas),i)
	

print 'MOEDAS:'
for i in [1,0.50,0.25,0.10,0.05,0.01]:
	notas = valor // i
	valor -= notas*i
	print '{} moedas de R$ {:.2f}'.format(int(notas),i)
	

