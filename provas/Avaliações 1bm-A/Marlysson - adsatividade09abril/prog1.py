# -*- coding:utf8 -*-

distancia = int(input('Distância: '))
combustivel     = float(input('Combustível: '))

consumo = distancia / combustivel

print 'Consumo médio: {:.3f} km/l'.format(consumo)
