# -*- coding: utf8 -*-

testes = int(input('Testes: '))
C,R,S = 0,0,0

for i in range(testes):
	dados = raw_input('QUANTIDADE - TIPO: ').split()
	quantidade,tipo = int(dados[0]) , dados[1].upper()

	if tipo == 'C':
		C += quantidade
	elif tipo == 'R':
		R += quantidade
	else:
		S += quantidade

total = C+R+S

print 'Total de cobaias: {}'.format(total)
print 'Total de Coelhos: {}'.format(C)
print 'Total de Ratos: {}'.format(R)
print 'Total de Sapos: {}'.format(S)

print 'Percentual de Coelhos: {:.2f}%'.format(C/float(total) * 100)
print 'Percentual de Ratos: {:.2f}%'.format(R/float(total) * 100)
print 'Percentual de Sapos: {:.2f}%'.format(S/float(total) * 100)
	
