valor = int (input('Valor:'))

cem = valor/100
cinquenta = valor%100/50
vinte = valor%100%50/20
dez = valor%100%50%20/10
cinco = valor%100%50%20%10/5
dois = valor%100%50%20%10%5/2
um = valor%100%50%20%10%5/1
cinquenta_cent = valor%100%50%20%10%5%1/0.50
vintecinco_cent = valor%100%50%20%10%5%1%0.5/0.25
dez_cent = valor%100%50%20%10%5%1%0.5%0.25/0.10
cinco_cent = valor%100%50%20%10%5%1%0.5%0.25%0.10/0.05
um_cent = valor%100%50%20%10%5%1%0.5%0.25%0.10%0.05/0.01


print'\nNOTAS: '

print'%.1f nota(s) de 100 R$'%cem
print'%.1f nota(s) de 50 R$'%cinquenta
print'%.1f nota(s) de 20 R$'%vinte
print'%.1f nota(s) de 10 R$'%dez
print'%.1f nota(s) de 5 R$'%cinco
print'%.1f nota(s) de 2 R$'%dois

print'\nMOEDAS'

print'%.1f Moeda(s) de 1 R$'%um
print'%.1f Moeda(s) de 50 cent'%cinquenta_cent 
print'%.1f Moeda(s) de 25 cent'%vintecinco_cent 
print'%.1f Moeda(s) de 10 cent'%dez_cent 
print'%.1f Moeda(s) de 5 cent'%cinco_cent 
print'%.1f Moeda(s) de 1 cent'%um_cent 

