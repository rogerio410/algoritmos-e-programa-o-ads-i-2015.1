Valor = float(input())

N100 = Valor / 100
R100 = Valor % 100
N50 = R100 / 50
R50 = R100 % 50
N20 = R50 / 20
R20 = R50 % 20
N10 = R20 / 10
R10 = R20 % 10
N5 = R10 / 5
R5 = R10 % 5
N2 = R5 / 2
R2 = R5 % 2
M1 = R2
RM1 = R2 % 1.00
M2 = RM1 / 0.50
RM2 = RM1 % 0.50
M3 = RM2 / 0.25
RM3 = RM2 % 0.25
M4 = RM3 / 0.10
RM4 = RM3 % 0.10
M5 = RM4 / 0.10
RM5 = RM4 % 0.10
M6 = RM5 / 0.01

print 'NOTAS: '
print '%d nota(s) de R$ 100.00' % N100
print '%d nota(s) de R$ 50.00' % N50
print '%d nota(s) de R$ 20.00' % N20
print '%d nota(s) de R$ 10.00' % N10
print '%d nota(s) de R$ 5.00' % N5
print '%d nota(s) de R$ 2.00' % N2
print 'MOEDAS: '
print '%d moeda(s) de R$ 1.00' % M1
print '%d moeda(s) de R$ 0.50' % M2
print '%d moeda(s) de R$ 0.25' % M3
print '%d moeda(s) de R$ 0.10' % M4
print '%d moeda(s) de R$ 0.05' % M5
print '%d moeda(s) de R$ 0.01' % M6

