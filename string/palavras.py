def has_no_e(palavra):
	if 'e' in palavra:
		return False
	else:
		return True

def has_no_e_turbo(palavra, letra):
	return not letra in palavra

def print_no_e_words(arquivo):
	arq = open(arquivo)
	for palavra in arq:
		if has_no_e(palavra):
			print palavra

def print_no_letter_turbo(arquivo, letra):
	arq = open(arquivo)
	for palavra in arq:
		if has_no_e_turbo(palavra, letra):
			print palavra

def avoids(palavra, letras):
	for elemento in letras:
		if elemento in palavra:
			return False
	return True

def list_avoids(arquivo):
	proibidas = raw_input('Letras Proib.: ')
	arq = open(arquivo)
	contador = 0
	for e in arq:
		if avoids(e, proibidas):
			#contador = contador + 1
			contador += 1
	print contador

def uses_only(palavra, letras):
	for letra in palavra:
		if not letra in letras:
			return False
	return True

