import time

def hello(variavel):
	print variavel

def multiplicar(num1, num2):
	print num2*num1

def contar_ocorrencia(palavra, letra):
	contador = 0
	for i in palavra:
		if letra == i:
			contador = contador + 1
	return contador

def contem(palavra, letra):
	if (contar_ocorrencia(palavra, letra) > 0):
		return True
	else:
		return False



def ver_palavras_sem(arquivo, letra, intevalo):
	#abrir o arquivo
	fin = open(arquivo)
	for linha in fin:
		time.sleep(intevalo)
		if not contem(linha, letra):
			print linha

def ver_palavras_sem_perc(arquivo, letra, intevalo):
	#abrir o arquivo
	fin = open(arquivo)
	#contados
	total = 0.0
	cont = 0.0
	for linha in fin:
		total = total + 1
		time.sleep(intevalo)
		if not contem(linha, letra):
			cont = cont + 1
			#print linha

	percental = (cont / total) * 100.0
	print ('Palavras sem a letra(%s): %.2f %%') % (letra, percental)

def contem_letras(palavra, letras):
	for i in letras:
		if contem(palavra, i):
			return False
	return True

def palavras_sem_letras(arquivo, letras):
	fin = open(arquivo)
	for palavra in fin:
		if contem_letras(palavra, letras):
			print palavra