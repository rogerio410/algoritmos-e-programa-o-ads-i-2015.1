def caixa_alta(param):
	return param.upper()

def caixa_baixa(param):
	resultado = param.lower()
	return resultado

def conta_caracter(texto, letra):
	qtd = texto.count(letra)
	return qtd

def conta_caracter2(texto, letra):
	aux = texto.split(letra)
	return len(aux)-1

def apagar_caracter(texto, letra):
	return texto.replace(letra, "")

def mostrar_repetidos(texto):
	texto = texto.lower()
	res = ""
	for i in texto:
		if (texto.count(i) > 1):
			if (i not in res):
				res = res + i
	return res

def remover_repetidos(texto):
	texto = texto.lower()
	repetidos = mostrar_repetidos(texto)
	for i in repetidos:
		texto = apagar_caracter(texto, i)
	return texto	

