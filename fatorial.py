def fat(n):
	if (not isinstance(n, int)):
		print 'Calculo permitido apenas para Inteiros'
		return None
	elif (n < 0):
		print 'Nao permitido para numero negativos'
		return None
	elif n == 0:
		return 1
	else:
		recurse = fat(n-1)
		result = n * recurse
		return result