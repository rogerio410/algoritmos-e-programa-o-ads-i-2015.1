from utils import *
"""Diversos consultas """

#Consulta para retornar Top Generica.
def top(escolas, uf='BR', n=10, rede=None, zona=None, r=False):

	if uf!='BR':
		escolas = filter_by_uf(escolas, uf)
	if rede:
		escolas = filter_by_rede(escolas, rede)
	if zona:
		escolas = filter_by_zona(escolas, zona)

	escolas.sort(_cmp_por_ranking, reverse=r)

	return escolas[:n]	

def find_by_name(escolas, txt):
	temp = []
	for e in escolas:
		if e[1].__contains__(txt):
			temp.append(e)
	if temp.__len__() > 0:
		temp.sort(_cmp_por_ranking)
	return temp

def avg_by_rede(escolas, uf):
	redes = ['E', 'F', 'M', 'P']
	medias = {}
	if uf!='BR':
		escolas = filter_by_uf(escolas, uf)
	for r in redes:
		temp = filter_by_rede(escolas, r)
		mr = avg(temp)
		medias[r]=mr
	return medias

#COMPARADORES
def _cmp_por_ranking(x,y):
	return cmp(x[0], y[0])

def _cmp_por_notafinal(x,y):
	return cmp(x[-1], y[-1])