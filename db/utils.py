
cabecalho = '\n\n #########----ENEM 2014----#######\n\n'
rodape = '\n\n********************************'
redes = {'F':'Federal', 'E':'Estadual', 'M':'Municipal', 'P':'Privada'}

def show_escolas(escolas):
	i = 1
	print cabecalho
	if not escolas:
		print "\t\t Nao ha escolas a exibir!"
	for e in escolas:
		print "%d \t %d \t %.2f -- %s(%s-%s) " %(i, e[0], e[-1], e[1],e[3], e[4])
		i+=1
	print rodape

def show_medias(medias):
	print cabecalho
	for k in medias:
		print redes[k],'\t',medias.get(k)


def show_comparacao_medias(uf1, uf2, medias1, medias2):
	print cabecalho
	print "REDE \t\t %s \t\t %s" % (uf1, uf2)
	for i in redes:
		print "%s \t %s \t %s" % (redes[i], medias1.get(i), medias2.get(i))
	print rodape

def filter_by_uf(escolas, uf):
	temp = []
	for e in escolas:
		if e[3] == uf:
			temp.append(e)
	return temp

def filter_by_zona(escolas, zona):
	temp = []
	for e in escolas:
		if e[7] == zona:
			temp.append(e)
	return temp

def filter_by_rede(escolas, rede):
	temp = []
	for e in escolas:
		#compara a primeira letra (E, F, M, P)
		if e[4][0] == rede:
			temp.append(e)
	return temp	

#calcula media geral
def avg(escolas):
	soma = 0.0

	if not escolas:
		return "0.00"

	for e in escolas:
		soma += e[-1]
	media = soma/escolas.__len__()
	return "%.2f" % media

#COMPARADORES
def _cmp_por_ranking(x,y):
	return cmp(x[0], y[0])

def _cmp_por_notafinal(x,y):
	return cmp(x[-1], y[-1])