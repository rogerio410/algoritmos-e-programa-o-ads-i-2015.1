"""
Cuida da importacao e normalizacao dos dados 
da planilha enem2014_nota_por_escola
*Ler o arquivo, faz o strip e segmenta os dados via split
*Fui definido que logo apos carregar botar em ordem alfabetica 
 ** Usou-se a funcao cmp no metoso sort da list
*Converter Ranking e Notas para int e float respectivamente
"""
def importar():
	f = open('enem2014_nota_por_escola.csv')
	#Strip
	escolas = []
	for l in f:
		escolas.append(l.strip().split(';'))
	#Usar o metodo sort par ordenar por nome
	escolas.sort(_cmp_por_nome)
	#Converter notas de str para float e cria nota Geral
	escolas = conversao_notas(escolas)
	return escolas

def conversao_notas(escolas):
	for i in range(escolas.__len__()):
		for j in range(7, 13):
			#Subs ',' por '.' e converter para float
			escolas[i][j] = float(escolas[i][j].replace(',','.'))
		#converter raking para int
		escolas[i][0] = int(escolas[i][0])
		escolas[i].append(escolas[i][7]+escolas[i][12])
	return escolas

def _cmp_por_nome(x,y):
	return cmp(x[1], y[1])

def _cmp_por_mat(x,y):
	return cmp(x[9],y[9])

def ordenar_escola(escolas):
	#TODO
	pass


