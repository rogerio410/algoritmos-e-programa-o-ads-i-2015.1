from importacao import *
from consultas import *
from utils import *
import os

def menu():
	#Lista com as escolas importadas
	escolas = []

	cabecalho = '\n #########----ENEM 2014----#######\n\n'
	opcoes = """		1 - Status 
		2 - Top 10 Brasil 
		3 - Top N por UF 
		4 - Por nome
		5 - Top N por UF e Rede
		6 - Media por UF e Rede
		7 - Comparar Medias por UF
		8 - Piores por UF(ou BR)
		0 - Sair"""
	rodape = '\n********************************'
	texto = cabecalho + opcoes + rodape

	escolas = importar()

	print texto
	op = input(' Digite sua opcao: ')
	while (op != 0):
		#Faz uma chamada para o Windows apagar o prompt
		os.system('cls')
		if op == 1:
			print '\nImportacao OK! \nEscolas Importadas: %d \n' % escolas.__len__()
		elif op == 2:
			show_escolas(top(escolas, n=10))
		elif op == 3:
			estado = raw_input('UF: ').upper()
			qtd = input('QTD: ')
			show_escolas(top(escolas, estado, qtd))
		elif op == 4:
			txt = raw_input('Nome ou Parte do Nome: ').upper()
			show_escolas(find_by_name(escolas, txt))
		elif op == 5:
			estado = raw_input('UF: ').upper()
			qtd = input('QTD: ')
			rede = raw_input('E / F / M / P: ').upper()
			show_escolas(top(escolas, estado, qtd, rede))
		elif op == 6:
			estado = raw_input('UF or BR: ').upper()
			medias = avg_by_rede(escolas, estado)
			show_medias(medias)
		elif op == 7:
			estado = raw_input('UF: ').upper()
			estado2 = raw_input('UF: ').upper()
			medias = avg_by_rede(escolas, estado)
			medias2 = avg_by_rede(escolas, estado2)
			show_comparacao_medias(estado, estado2, medias, medias2)
		elif op == 8:
			estado = raw_input('UF or BR: ').upper()
			show_escolas(top(escolas, estado, r=True))
		else:
			print 'Opcao invalida'


		print texto
		op = input(' Digite sua opcao: ')
		
	print 'Sessao encerrada.'

menu()